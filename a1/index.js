/* 
	Create a trainer object using object literals.

	Initialize/add the following trainer object properties:
		- Name (String)
		- Age (Number)
		- Pokemon (Array)
		- Friends (Object with Array values for properties)

	Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
*/

	let trainer = {
		name: "Ash Catsup",
		age: 12,
		pokemon: ["Pikachu", "Charmander", "Bulbasaur", "Squirtle"],
		friends: ["Meste", "Brack"],
		talk: function(){
			console.log("Pikachu! I choose you!")
		}
		}



	
	
/*
	Access the trainer object properties using dot and square bracket notation.
*/
		console.log("Trainer Name: " + trainer.name);
		console.log("Trainer Age: " + trainer.age);
		console.log("Trainer's Pokemon: " + trainer.pokemon);
		console.log("Trainer's Friends: " + trainer.friends);
		
/*
	Invoke/call the trainer talk object method.
*/

		trainer.talk();
/*
	Create a constructor for creating a pokemon with the following properties:
		- Name (Provided as an argument to the contructor)
		- Level (Provided as an argument to the contructor)
		- Health (Create an equation that uses the level property)
		- Attack (Create an equation that uses the level property)
*/
		
		function pokemon(namePokemon, level){
			this.namePokemon = namePokemon;
			this.level = level;
			this.health = level * 2;
			this.attack = level * 1.5;
			this.tackle = function(target){
				console.log(this.namePokemon + " tackled " + target.namePokemon);
			target.health -= this.attack;
			console.log(target.namePokemon + " health is now reduced to " + target.health);
			if(target.health <= 0){
				target.faint()
			}
			}
			this.faint = function(){
				console.log(this.namePokemon + " fainted.")
			}
		}


/*
	Create/instantiate several pokemon object from the constructor with varying name and level properties.
*/
	let magikarp = new pokemon("Magikarp", 100);
	let gyarados = new pokemon("Gyarados", 5);




/*
Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
*/

	
/*
Create a faint method that will print out a message of targetPokemon has fainted.
*/


/*
Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
*/


/*
Invoke the tackle method of one pokemon object to see if it works as intended.
*/


/*
Create a git repository named S23.
*/


/*
Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
*/


/*
Add the link in Boodle.
*/